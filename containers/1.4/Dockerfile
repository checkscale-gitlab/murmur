FROM hairyhenderson/gomplate:v3.10.0-slim@sha256:13aff688f4923f5f13712b3e782470a8d5cf58a09778d96d81f3d8c72b92608a as gomplate

#hadolint ignore=DL3007
FROM registry.gitlab.com/akontainers/apkbuild-tools:latest@sha256:8407dc56d7b5d58721051a72bd530acb0139915f3cb8ffd9deb878376919ce95 as apkbuild-tools

FROM alpine:3.16.0@sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c as builder
COPY --from=apkbuild-tools /usr/sbin/builder /usr/sbin/builder
RUN builder prepare
COPY --chown=abuild:abuild builder/abuild /home/abuild
USER abuild
RUN builder build

FROM alpine:3.16.0@sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c as copier

COPY --from=gomplate /gomplate /usr/sbin/gomplate
COPY --from=builder /home/abuild/.abuild/*.rsa.pub /etc/apk/keys/
COPY --from=builder /home/abuild/packages/abuild/ /packages/

# renovate: datasource=github-tags depName=mumble-voip/mumble
ENV MURMUR_VERSION=1.4.230

RUN set -eux; \
      echo '/packages' >> /etc/apk/repositories; \
      apk add --no-cache \
        "murmur=${MURMUR_VERSION}-r99" \
      ; \
      rm -rf /packages;

FROM scratch

ENV TZ=UTC

VOLUME /database
EXPOSE 64738/tcp 64738/udp
ENTRYPOINT ["docker-entrypoint"]
CMD ["murmur", "-ini", "/etc/murmur.ini"]

ENV MURMUR_UNAME='murmur' \
    MURMUR_DATABASE='/database/murmur.sqlite'

COPY --from=copier / /
COPY rootfs /
