#!/usr/bin/env sh

set -eu

exportVariable() {
  if [ "$#" -eq 1 ]; then
    exportedVariableName=$1
    lookupVariableName=$1
    defaultVariableValue=''
  elif [ "$#" -eq 2 ]; then
    exportedVariableName=$1
    lookupVariableName=$1
    defaultVariableValue=$2
  elif [ "$#" -eq 3 ]; then
    exportedVariableName=$1
    lookupVariableName=$2
    defaultVariableValue=$3
  else
    return
  fi
  eval "export $exportedVariableName=\${$lookupVariableName:-$defaultVariableValue}"
}

# shellcheck disable=SC1091
unsetVariables() {
  . /usr/sbin/support/log-events

  for argument in "$@"; do
    logMessage "Unset variables with regexp $argument"
    # shellcheck disable=SC2046
    unset $(awk -v regexp="$argument" '
      BEGIN {
        for(envVar in ENVIRON) {
          if(envVar ~ regexp) {
            print envVar
          }
        }
      }
    ')
  done
}
